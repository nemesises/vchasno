﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;
using vchasnocrm.Repository;

namespace vchasnocrm.Controllers
{
    public class ProposalsController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();
        private ProposalsRepository _repository = new ProposalsRepository();

        public JsonResult Index()
        {

            var allproposals = _repository.GetAllProposals(); 
            return Json(allproposals.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeStatus(int id, string status)
        {
            var changeStatus = _repository.ChangeStatus(id,status);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult ProposalRegions(string email)
        {

           
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

            string id_val = id.Id;

            var region_id = db.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

            int? reg_id = region_id.Region_Id;

            var proposal = db.Proposals.Where(x => x.Project.City.Region.Id == reg_id)
                .Select(x => new
                {
                    Project = x.Project.Name,
                    WorkTime = x.WorkTime,
                    Quantity = x.Quantity,
                    Price = x.Price,
                    Service = x.Service.Name,
                    DateFrom = x.Date,
                    DateTo = x.Date_to,
                    WorkTimeTo = x.WorkTimeTo,
                    Id = x.Id,
                    EditingDate = x.CreatingDate,
                    Status = x.Status

                }).ToList();


            return Json(proposal, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ProposalCountry(string email)
        {


            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

            string id_val = id.Id;

            var region_id = db.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

            int? reg_id = region_id.Country_Id;

            var proposal = db.Proposals.Where(x => x.Project.City.Region.Area.Country_id == reg_id)
                .Select(x => new
                {
                    Project = x.Project.Name,
                    WorkTime = x.WorkTime,
                    Quantity = x.Quantity,
                    Price = x.Price,
                    Service = x.Service.Name,
                    DateFrom = x.Date,
                    DateTo = x.Date_to,
                    WorkTimeTo = x.WorkTimeTo,
                    Id = x.Id,
                    EditingDate = x.CreatingDate,
                    Status = x.Status

                }).ToList();


            return Json(proposal, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProposalArea(string email)
        {


            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

            string id_val = id.Id;

            var region_id = db.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

            int? reg_id = region_id.Area_id;

            var proposal = db.Proposals.Where(x => x.Project.City.Region.Area_id == reg_id)
                .Select(x => new
                {
                    Project = x.Project.Name,
                    WorkTime = x.WorkTime,
                    Quantity = x.Quantity,
                    Price = x.Price,
                    Service = x.Service.Name,
                    DateFrom = x.Date,
                    DateTo = x.Date_to,
                    WorkTimeTo = x.WorkTimeTo,
                    Id = x.Id,
                    EditingDate = x.CreatingDate,
                    Status = x.Status

                }).ToList();


            return Json(proposal, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ServiceFilter(int serviceId)
        {
            var filtered = db.Proposals.Where(x=> x.Service_Id == serviceId).Select(x => new
            {
                Project = x.Project.Name,
                WorkTime = x.WorkTime,
                Quantity = x.Quantity,
                Price = x.Price,
                Service = x.Service.Name,
                DateFrom = x.Date,
                DateTo = x.Date_to,
                WorkTimeTo = x.WorkTimeTo,
                Id = x.Id,
                EditingDate = x.CreatingDate,
                Status = x.Status

            }).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProjectFilter(int projectId)
        {
            var filtered = db.Proposals.Where(x =>x.Project_id == projectId).Select(x => new
            {
                Project = x.Project.Name,
                WorkTime = x.WorkTime,
                Quantity = x.Quantity,
                Price = x.Price,
                Service = x.Service.Name,
                DateFrom = x.Date,
                DateTo = x.Date_to,
                WorkTimeTo = x.WorkTimeTo,
                Id = x.Id,
                EditingDate = x.CreatingDate,
                Status = x.Status

            }).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DateFilter(DateTime date)
        {
            var filtered = db.Proposals.Where(x => x.Date == date).Select(x => new
            {
                Project = x.Project.Name,
                WorkTime = x.WorkTime,
                Quantity = x.Quantity,
                Price = x.Price,
                Service = x.Service.Name,
                DateFrom = x.Date,
                DateTo = x.Date_to,
                WorkTimeTo = x.WorkTimeTo,
                Id = x.Id,
                EditingDate = x.CreatingDate,
                Status = x.Status

            }).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        public JsonResult StatusFilter(string status)
        {
            var filtered = db.Proposals.Where(x => x.Status == status).Select(x => new
            {
                Project = x.Project.Name,
                WorkTime = x.WorkTime,
                Quantity = x.Quantity,
                Price = x.Price,
                Service = x.Service.Name,
                DateFrom = x.Date,
                DateTo = x.Date_to,
                WorkTimeTo = x.WorkTimeTo,
                Id = x.Id,
                EditingDate = x.CreatingDate,
                Status = x.Status

            }).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CityFilter(int city)
        {
            var filtered = db.Proposals.Where(x => x.Project.City_Id == city).Select(x => new
            {
                Project = x.Project.Name,
                WorkTime = x.WorkTime,
                Quantity = x.Quantity,
                Price = x.Price,
                Service = x.Service.Name,
                DateFrom = x.Date,
                DateTo = x.Date_to,
                WorkTimeTo = x.WorkTimeTo,
                Id = x.Id,
                EditingDate = x.CreatingDate,
                Status = x.Status

            }).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FineInProposal (int proposalId)
        {
            var fine = db.ProposalToFines.Where(x => x.Proposal_Id == proposalId).Select(x => new
            {
                Fine = x.Fine.Name,
                Price = x.Fine.Price,
                Comment = x.Comment
            }).ToList();
            return Json(fine, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProposalProject(string email)
        {
          
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

            string id_val = id.Id;

            var project_id = db.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

            int? reg_id = project_id.Project_Id;
            var proposals = db.Proposals.OrderByDescending(s => s.Id).Where(x => x.Project.Id == reg_id)
               .Select(x => new
               {
                   Project = x.Project.Name,
                   WorkTime = x.WorkTime,
                   Quantity = x.Quantity,
                   Price = x.Price,
                   Service = x.Service.Name,
                   DateFrom = x.Date,
                   DateTo = x.Date_to,
                   WorkTimeTo = x.WorkTimeTo,
                   Id = x.Id,
                   EditingDate = x.CreatingDate,
                   Status = x.Status

               }).ToList();
            return Json(proposals, JsonRequestBehavior.AllowGet);

        }

        public JsonResult Changer(int serviceId) {
            var feeresult = db.Services.Where(x => x.Id == serviceId)
                .Select(x=> new {
                    rate = Math.Round(x.Price, 2),
                })
                .ToList();
            return Json(feeresult, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteProposal(int id)
        {
            var deleteproposal= _repository.DeleteProposal(id);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult AddProposal(int project_id, string worktime,string worktimeto, int quantity, int service_id, string date_to,string date_from, string rate, string status)
        {

            var addProposal = _repository.AddProposal(project_id, worktime, worktimeto, quantity, service_id, date_to, date_from, rate,status);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

      
    }
}

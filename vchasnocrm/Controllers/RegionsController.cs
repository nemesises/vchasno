﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;

namespace vchasnocrm.Controllers
{
    public class RegionsController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();

        // GET: Regions
       
        public JsonResult AddRegion(string name, int areaId)
        {
            Region reg = new Region()
            {
                Name = name,
                Area_id = areaId
               
                
            };
            db.Regions.Add(reg);
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult UpdateRegion(int id,string name){
            var region = db.Regions.Find(id);
            region.Name = name;
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
        public JsonResult Index()
        {
            var regions = db.Regions.Select(x=> new {
                Name = x.Name,
                Id=x.Id
            }).ToList();
            return Json(regions, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteRegion(int id)
        {
            var reg = db.Regions.Find(id);
            db.Regions.Remove(reg);
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
    }
}

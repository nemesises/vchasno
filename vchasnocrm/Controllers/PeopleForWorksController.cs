﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;

namespace vchasnocrm.Controllers
{
    public class PeopleForWorksController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();


        // GET: PeopleForWorks
        public JsonResult Index()
        {
            var people = db.PeopleForWorks.Select(x=> new {

                Id = x.Id,
                FIO = x.FIO,
                Position = x.Position,
                Birthday = x.Birthday,
                TelephoneNumber = x.TelephoneNumber,
                WorkTime = x.WorkTime,
                Adress = x.Adress,
                INN = x.INN,
                PassportData = x.PassportData,
                MedicalBook = x.MedicalBook,
                Region = x.Region


            }).ToList();
            return Json(people, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeletePeople(int id)
        {
            var city = db.PeopleForWorks.Find(id);
            db.PeopleForWorks.Remove(city);
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult PeopleInProposals() {
            var data = db.ProposalsToPeoples.Select(x => new
            {
                FIO = x.PeopleForWork.FIO,
                Proposal = x.Proposal.Id
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddPeopleToProposal(int peopleId,int proposalId)
        {
            ProposalsToPeople prop = new ProposalsToPeople()
            {
                People_Id = peopleId,
                Proposal_Id = proposalId
            };
            db.ProposalsToPeoples.Add(prop);
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
        public JsonResult AddPeople(string fio, string position, string region,string birthday, string telephone,string worktime, string adress, string passportdata,string medicalbook, string INN, string bookending)
        {
            DateTime Birthday = DateTime.Parse(birthday);
            DateTime BookEnding = DateTime.Parse(bookending);
            PeopleForWork pw = new PeopleForWork()
            {
               FIO = fio,
               Birthday = Birthday,
               TelephoneNumber =  telephone,
               WorkTime = worktime,
               Adress = adress,
               PassportData = passportdata,
               MedicalBook = medicalbook,
               INN = INN,
               BookEnding = BookEnding,
               Position = position,
               Region = region
            };
            db.PeopleForWorks.Add(pw);
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;
using vchasnocrm.Repository;

namespace vchasnocrm.Controllers
{
    public class AreasController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();
        private AreaRepository _repository = new AreaRepository();
        // GET: Areas
        public JsonResult Index()
        {
            var allareasresult = _repository.GetAllAreas();
            return Json(allareasresult.ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddArea(string name, int country_id)
        {
            var addresult = _repository.AddArea(name, country_id);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

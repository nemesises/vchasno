﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;
using vchasnocrm.Repository;

namespace vchasnocrm.Controllers
{
    public class CitiesController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();
        private CitiesRepository _repository = new CitiesRepository();

        public ActionResult AddCityPartial() {
            return PartialView("Manage/Partials/CityModalPartial");
        }
        // GET: Cities
        public JsonResult Index()
        {
            var allcitiesresult = _repository.GetAllCities();
            return Json(allcitiesresult.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateCity(int id,string name){
            var updateresult = _repository.UpdateCity(id,name);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult AddCity(string name, int region_id)
        {
            var addresult = _repository.AddCity(name,region_id);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

       public JsonResult DeleteCity(int id)
        {
            var deleteresult = _repository.DeleteCity(id);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult CitiesFilterResult()
        {
            var cities = db.Cities
             .Select(x => new
             {
                 Id = x.Id,
                 Name = x.Name

             }).ToList();

            return Json(cities, JsonRequestBehavior.AllowGet);
        }
    }
}

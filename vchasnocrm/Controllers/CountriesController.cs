﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;
using vchasnocrm.Repository;

namespace vchasnocrm.Controllers
{
    public class CountriesController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();
        private CountryRepository _repository = new CountryRepository();
        // GET: Countries
        public JsonResult Index()
        {
            var allcountriesresult = _repository.GetAllCountries();
            return Json(allcountriesresult.ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddCountry(string name)
        {
            var addresult = _repository.AddCountry(name);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

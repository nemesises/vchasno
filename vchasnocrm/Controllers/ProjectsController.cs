﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;
using vchasnocrm.Repository;

namespace vchasnocrm.Controllers
{
    public class ProjectsController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();
        private ProjectsRepository _repository = new ProjectsRepository();
        // GET: Projects
        public JsonResult Index()
        {
            var allprojectResult = _repository.GetAllProjects();
            return Json(allprojectResult.ToList(),JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateProject(int id,string name){
            var updateResult = _repository.UpdateProject(id,name); 
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult AddProject(string name, int city_id)
        {
            var addResult = _repository.AddProject(name,city_id);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult DeleteProject(int id)
        {
            var deleteResult = _repository.DeleteProject(id);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }


        public JsonResult ProjectsFilterResult()
        {
            var projects = db.Projects
             .Select(x => new
             {
                 Id = x.Id,
                 Name = x.Name

             }).ToList();

            return Json(projects, JsonRequestBehavior.AllowGet);
        }
    }
}

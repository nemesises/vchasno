﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;

namespace vchasnocrm.Controllers
{
    public class ServicesController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();

        // GET: Services
        public ActionResult Index()
        {
            var services = db.Services.Select(x => new
            {
                Name = x.Name,
                Price = Math.Round(x.Price,2),
                Id = x.Id
            }).ToList();
            return Json(services, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddService(string name, string fee, int country_id)
        {
           // decimal fee_one = Decimal.Parse(fee);
            //string too = fee_one.ToString();
            float Fee = float.Parse(fee, System.Globalization.CultureInfo.InvariantCulture);
            Service srv = new Service()
            {
               Name = name,
               Price = Fee,
               Country_id = country_id 

            };
            db.Services.Add(srv);
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
        public JsonResult ServicesFilterResult()
        {
            var services = db.Services
             .Select(x => new
             {
                 Id = x.Id,
                 Name = x.Name

             }).ToList();

            return Json(services, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteService(int id)
        {
            var serv = db.Services.Find(id);
            db.Services.Remove(serv);
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
    }
}

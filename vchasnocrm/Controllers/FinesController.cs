﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;
using vchasnocrm.Repository;

namespace vchasnocrm.Controllers
{
    public class FinesController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();

        private FinesRepository _repository = new FinesRepository();
        // GET: Fines
        public ActionResult Index()
        {
            var allfinesresult = _repository.GetAllFines();
            return Json(allfinesresult.ToList(), JsonRequestBehavior.AllowGet);
        }

      
        public JsonResult AddFine(string name, float price, int country_Id)
        {
            var addfineresult = _repository.AddFine(name, price, country_Id);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult DeleteFine(int id)
        {
            var deletedineresult = _repository.DeleteFine(id);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

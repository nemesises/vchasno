﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using vchasnocrm.Models;

namespace vchasnocrm.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private vchasnopersonalEntities db = new vchasnopersonalEntities();

        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                return View();//supposing it is the view for admin
            }
            if (User.IsInRole("VchasnoManager")) //Vchasno Manager
            {
                return View("IndexVchasnoManager");
            }
            if (User.IsInRole("regional manager")) //Regional Manager
            {
               
                return View("IndexRegionManager");
            }
            if (User.IsInRole("Country Manager")) //Country Manager
            {

                return View("IndexCountryManager");
            }
            if (User.IsInRole("area manager")) //Area Manager
            {

                return View("IndexAreaManager");
            }
            else {
                return View("IndexProjectManager");
            }
        }


       

        public ActionResult AddCity()
        {
            ViewBag.Region = new SelectList(db.Regions, "Id", "Name").ToList();
            return PartialView("CityModalPartial");
        }
        public ActionResult AddInfoToPropPeople()
        {
            return PartialView("AddTimeProposal");
        }
        public ActionResult AddRegion()
        {
            ViewBag.Area = new SelectList(db.Areas, "Id", "Name").ToList();
            return PartialView("AddRegion");
        }
        public ActionResult AddCountry(){
            return PartialView("AddCountry");
        }
        public ActionResult AddArea(){
            ViewBag.Country = new SelectList(db.Countries, "Id", "Name").ToList();
            return PartialView("AddArea");
        }
        public ActionResult AddService()
        {
            ViewBag.Country = new SelectList(db.Countries, "Id", "Name").ToList();
            return PartialView("AddService");
        }
       
        public ActionResult ProposalToFineAdd()
        {
            ViewBag.Fine_Id = new SelectList(db.Fines, "Id", "Name");
            ViewBag.Proposal_Id = new SelectList(db.Proposals, "Id", "Id");
            return PartialView("AddProposalToFine");
        }
        public ActionResult AddWorker()
        {
            ViewBag.Region = new SelectList(db.Regions, "Id", "Name").ToList();
            return PartialView("AddPeopleWorkers");
        }
        public ActionResult AddProject()
        {
            ViewBag.City = new SelectList(db.Cities, "Id", "Name").ToList();
            return PartialView("ProjectModalPartial");
        }
        public ActionResult AddProposal()
        {
           
            ViewBag.Service = new SelectList(db.Services, "Id", "Name").ToList();
            ViewBag.Project = new SelectList(db.Projects, "Id", "Name").ToList();
            return PartialView("AddProposal");
        }

        public ActionResult AddProposalManager()
        {
           
          
            return PartialView("AddProposalManager");

        }
        public ActionResult AddProposalManagerRegion()
        {

            return PartialView("AddProposalManagerRegion");

        }
        public ActionResult AddProposalManagerCountry()
        {
            return PartialView("AddProposalManagerCountry");
        }

        public ActionResult AddProposalManagerArea()
        {
            return PartialView("AddProposalManagerArea");
        }

        #region Population
        public JsonResult PopulateProjects(string email)
        {
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

            string id_val = id.Id;

            var project_id = db.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

            int? reg_id = project_id.Project_Id;
            var projects = db.Projects.Where(x => x.Id == reg_id)
              .Select(x => new
              {
                  Id = x.Id,
                  Name = x.Name

              }).ToList();

            return Json(projects, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PopulateProjectsRegions(string email)
        {
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

            string id_val = id.Id;

            var project_id = db.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

            int? reg_id = project_id.Region_Id;
            var projects = db.Projects.Where(x => x.City.Region_Id == reg_id)
              .Select(x => new
              {
                  Id = x.Id,
                  Name = x.Name

              }).ToList();

            return Json(projects, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PopulateProjectsCountry(string email)
        {
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

            string id_val = id.Id;

            var country_id = db.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

            int? reg_id = country_id.Country_Id;
            var projects = db.Projects.Where(x => x.City.Region.Area.Country.Id == reg_id)
              .Select(x => new
              {
                  Id = x.Id,
                  Name = x.Name

              }).ToList();

            return Json(projects, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PopulateProjectsArea(string email)
        {
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

            string id_val = id.Id;

            var area_id = db.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

            int? reg_id = area_id.Area_id;
            var projects = db.Projects.Where(x => x.City.Region.Area.Id == reg_id)
              .Select(x => new
              {
                  Id = x.Id,
                  Name = x.Name

              }).ToList();

            return Json(projects, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PopulateServiceCountry(string email)
        {
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();
            int? country_id = id.Country_Id;
            var services = db.Services.Where(x => x.Country_id == country_id)
                .Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PopulateServiceArea(string email)
        {
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();
            int? area_id = id.Area_id;

            var country_id = db.Areas.Where(x => x.Id == area_id).Select(x => new { countryId = x.Country_id }).FirstOrDefault();

            int? country = country_id.countryId;
            var services = db.Services.Where(x => x.Country_id == country)
                .Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PopulateServiceRegion(string email)
        {
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();
            int? region_id = id.Region_Id;

            var regionid = db.Regions.Where(x => x.Id == region_id).Select(x => new { areaId = x.Area_id }).FirstOrDefault();

            int? area = regionid.areaId;
            var country_id = db.Areas.Where(x => x.Id == area).Select(x => new { countryId = x.Country_id }).FirstOrDefault();
            int? country = country_id.countryId;
            var services = db.Services.Where(x => x.Country_id == country)
                .Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PopulateServiceProject(string email)
        {
            var id = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();
            int? project_id = id.Project_Id;

            var cityid = db.Projects.Where(x => x.Id == project_id).Select(x => new { cityId = x.City_Id }).FirstOrDefault();
            int? city = cityid.cityId;

            var region_Id = db.Cities.Where(x => x.Id == city).Select(x => new { regionId = x.Region.Id }).FirstOrDefault();

            int? region = region_Id.regionId;

            var regionid = db.Regions.Where(x => x.Id == region).Select(x => new { areaId = x.Area_id }).FirstOrDefault();

            int? area = regionid.areaId;
            var country_id = db.Areas.Where(x => x.Id == area).Select(x => new { countryId = x.Country_id }).FirstOrDefault();
            int? country = country_id.countryId;
            var services = db.Services.Where(x => x.Country_id == country)
                .Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult AddManagerToRegion() {
            ViewBag.Manager = new SelectList(db.AspNetUsers, "Id", "Email").ToList();
            ViewBag.Region = new SelectList(db.Regions, "Id", "Name").ToList();
            return PartialView("AddManagerToRegion");
        }

        public ActionResult AddManagerToProject() {
            ViewBag.Manager = new SelectList(db.AspNetUsers, "Id", "Email").ToList();
            ViewBag.Project = new SelectList(db.Projects, "Id","Name");
            return PartialView("AddManagerToProject");
        }
        public ActionResult AddManagerToArea()
        {
            ViewBag.Manager = new SelectList(db.AspNetUsers, "Id", "Email").ToList();
            ViewBag.Area = new SelectList(db.Areas, "Id", "Name").ToList();
            return PartialView("AddManagerToArea");
        }
        public ActionResult AddManagerToCountry()
        {
            ViewBag.Manager = new SelectList(db.AspNetUsers, "Id", "Email").ToList();
            ViewBag.Country = new SelectList(db.Countries, "Id", "Name").ToList();
            return PartialView("AddManagerToCountry");
        }
        public ActionResult AddPeopleToProposal()
        {
            ViewBag.Worker = new SelectList(db.PeopleForWorks, "Id", "FIO").ToList();
            ViewBag.Proposal = new SelectList(db.Proposals, "Id", "Id").ToList();
            return PartialView("AddPeopleToProposal");
        }
        public ActionResult AddFinePartial()
        {
            ViewBag.Country = new SelectList(db.Countries, "Id", "Name").ToList();
            return PartialView("AddFinePartial");
        }
        public ActionResult ViewPeople()
        {
            
            return PartialView("/Partials/WorkingPeopleModal");
        }
        public ActionResult ViewFines()
        {

            return PartialView("/Partials/FinesModal");
        }
        public ActionResult ViewCities()
        {
            return PartialView("/Partials/CitiesModal");
        }
        public ActionResult ViewRegions()
        {
            return PartialView("/Partials/RegionsModal");
        }
        public ActionResult ViewProjects()
        {
            return PartialView("/Partials/ProjectsModal");
        }
        public ActionResult ViewServices()
        {
            return PartialView("/Partials/ServicesModal");
        }
        
       public ActionResult ViewPeopleProposals()
        {
            return PartialView("AllPeopleProposals");
        }
        public ActionResult ViewFinesProposals()
        {
            return PartialView("AllFinesProposals");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }


    }
}
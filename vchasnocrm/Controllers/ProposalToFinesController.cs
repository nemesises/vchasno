﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;

namespace vchasnocrm.Controllers
{
    public class ProposalToFinesController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();

        // GET: ProposalToFines
       /* public ActionResult Index()
        {
            var proposalToFines = db.ProposalToFines.Include(p => p.Fine).Include(p => p.Proposal);
            return View(proposalToFines.ToList());
        }*/

       
        // GET: ProposalToFines/Create
        public JsonResult Add(int proposal_Id, int fine_Id, string comment)
        {
            ProposalToFine prtfn = new ProposalToFine()
            {
                Proposal_Id = proposal_Id,
                Fine_Id = fine_Id,
                Comment = comment
            };
            db.ProposalToFines.Add(prtfn);
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

       
      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

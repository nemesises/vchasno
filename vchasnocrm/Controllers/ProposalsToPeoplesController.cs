﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;

namespace vchasnocrm.Controllers
{
    public class ProposalsToPeoplesController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();

        // GET: ProposalsToPeoples
      

        public JsonResult PeopleInProposals(int proposalId)
        {
            var people = db.ProposalsToPeoples.Where(p => p.Proposal_Id == proposalId).Select(x => new
            {
                Id = x.Id,
                Name = x.PeopleForWork.FIO,
                Comment = x.Comment,
                WorkFrom = x.WorkFrom,
                WorkTo = x.WorkTo,
                BreakFrom = x.BreakFrom,
                BreakTo =x.BraekTo,
                Status = x.HoursConfirmation
            }).ToList();

            return Json(people, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddInfo(string workFrom, string workTo,string breakFrom, string breakTo, string Comment, int id)
        {
            var person = db.ProposalsToPeoples.Where(x => x.Id == id).FirstOrDefault();
            person.WorkTo = workTo;
            person.WorkFrom = workFrom;
            person.BreakFrom = breakFrom;
            person.BraekTo = breakTo;
            person.Comment = Comment;

            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        public JsonResult ConfirmTime(int id)
        {
            var person = db.ProposalsToPeoples.Where(x => x.Id == id).FirstOrDefault();
            person.HoursConfirmation = "Подтверждено";
            db.SaveChanges();
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

        // GET: ProposalsToPeoples/Details/5
       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

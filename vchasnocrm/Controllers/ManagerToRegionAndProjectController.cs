﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using vchasnocrm.Models;
using vchasnocrm.Repository;

namespace vchasnocrm.Controllers
{
    public class ManagerToRegionAndProjectController : Controller
    {
        private vchasnopersonalEntities db = new vchasnopersonalEntities();
        private ManagerToRegionAndProjectRepository _repository = new ManagerToRegionAndProjectRepository();
        // GET: ManagerToRegion
        public JsonResult AddToRegion(string managerId, int regionId)
        {

            var reg = _repository.AddToRegion(managerId, regionId);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
        public JsonResult AddToProject(string managerId, int projectId) {


            var reg = _repository.AddToProject(managerId, projectId);
          return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
        public JsonResult AddToArea(string managerId, int areaId)
        {

            var ar = _repository.AddToArea(managerId, areaId);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }
        public JsonResult AddToCountry(string managerId, int countryId)
        {


            var cnt = _repository.AddToCountry(managerId, countryId);
            return Json(new { Result = "Success", Message = "Saved Successfully" });
        }

    }
}
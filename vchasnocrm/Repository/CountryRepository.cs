﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vchasnocrm.Models;
using vchasnocrm.ViewModels;

namespace vchasnocrm.Repository
{
    public class CountryRepository
    {
        public List<CountryViewModel> GetAllCountries()
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                List<CountryViewModel> allCountries = new List<CountryViewModel>();
                var countries = ctx.Countries.ToList();
                for (int i = 0; i < countries.Count; i++)
                {
                    allCountries.Add(new CountryViewModel
                    {
                        Name = countries[i].Name
                    
                    });
                }
                return allCountries;
            }
        }
        public string AddCountry(string name)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                Country country = new Country()
                {
                    Name = name
                };
                ctx.Countries.Add(country);
                ctx.SaveChanges();
                message = "Added";
                return message;
            }
        }
    }
}
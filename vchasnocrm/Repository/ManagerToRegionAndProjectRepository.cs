﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vchasnocrm.Models;

namespace vchasnocrm.Repository
{
    public class ManagerToRegionAndProjectRepository
    {
        public string AddToRegion(string managerId, int regionId)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var reg = ctx.AspNetUsers.Find(managerId);
                reg.Region_Id = regionId;
                ctx.SaveChanges();
                message = "Added";
                return message;
            }
        }

        public string AddToProject(string managerId, int projectId)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var reg = ctx.AspNetUsers.Find(managerId);
                reg.Project_Id = projectId;
                ctx.SaveChanges();
                message = "Added";
                return message;
            }
        }
        public string AddToArea(string managerId, int areaId)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var reg = ctx.AspNetUsers.Find(managerId);
                reg.Area_id = areaId;
                ctx.SaveChanges();
                message = "Added";
                return message;
            }
        }
        public string AddToCountry(string managerId, int countryId)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var reg = ctx.AspNetUsers.Find(managerId);
                reg.Country_Id = countryId;
                ctx.SaveChanges();
                message = "Added";
                return message;
            }
        }
    }
}
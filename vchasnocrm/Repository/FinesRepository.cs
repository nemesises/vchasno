﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vchasnocrm.Models;

namespace vchasnocrm.Repository
{
    public class FinesRepository
    {
        public string AddFine(string name, float price, int country_Id)
        {
            using(var ctx =  new vchasnopersonalEntities())
            {
                string message;

                Fine fn = new Fine()
                {
                    Name = name,
                    Price = price,
                    Country_id = country_Id
                    
                };
                ctx.Fines.Add(fn);
                ctx.SaveChanges();
                message = "Added";
                return message;
            }
        }

        public string DeleteFine (int id)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var fn = ctx.Fines.Find(id);
                ctx.Fines.Remove(fn);
                ctx.SaveChanges();
                message = "Deleted";
                return message;
            }
        }

        public List<Fine> GetAllFines()
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                List<Fine> allFines = new List<Fine>();
                var fines = ctx.Fines.ToList();
                for (int i = 0; i < fines.Count; i++)
                {
                    allFines.Add(new Fine
                    {
                        Name = fines[i].Name,
                        Price = fines[i].Price
                    });
                }

                return allFines;

            }
        }
      
    }
}
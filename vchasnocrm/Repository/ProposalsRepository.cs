﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vchasnocrm.Models;
using vchasnocrm.ViewModels;

namespace vchasnocrm.Repository
{
    public class ProposalsRepository
    {
        public List<ProposalViewModel> GetAllProposals()
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                List<ProposalViewModel> allProposals = new List<ProposalViewModel>();
                var proposals = ctx.Proposals.OrderByDescending(s => s.Id).ToList();
                for (int i = 0; i < proposals.Count; i++)
                {
                    allProposals.Add(new ProposalViewModel
                    {
                        Project = proposals[i].Project.Name,
                        WorkTime = proposals[i].WorkTime,
                        Quantity = proposals[i].Quantity,
                        Price = proposals[i].Price,
                        Service = proposals[i].Service.Name,
                        DateFrom = proposals[i].Date,
                        DateTo = proposals[i].Date_to,
                        WorkTimeTo = proposals[i].WorkTimeTo,
                        Id = proposals[i].Id,
                        EditingDate = proposals[i].CreatingDate,
                        Status = proposals[i].Status,
                        DisablingDate = proposals[i].Disabling_Time,
                        City = proposals[i].Project.City.Name
                    });
                }
                return allProposals;
            }
        }

        public string ChangeStatus(int id, string status)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                if (status == "Заявка отменена" || status == "Отклонено региональным менеджером" || status == "Отклонено территориальным менеджером" || status == "Отменено менеджером Vchasno")
                {
                    var proposal = ctx.Proposals.Find(id);
                    proposal.Status = status;
                    proposal.Disabling_Time = DateTime.Now;
                    ctx.SaveChanges();
                    message = "Status Changed";
                }
                else
                {
                    var proposal = ctx.Proposals.Find(id);
                    proposal.Status = status;
                    ctx.SaveChanges();
                    message = "Status Changed";
                }
               
               return message;
            }
        }


        public List<ProposalViewModel> GetAllProposalsRegions(string email)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                List<ProposalViewModel> allProposalsRegions = new List<ProposalViewModel>();
                var id = ctx.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

                string id_val = id.Id;

                var region_id = ctx.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

                int? reg_id = region_id.Region_Id;
                var proposals = ctx.Proposals.Where(x => x.Project.City.Region_Id == reg_id).ToList();
                for (int i = 0; i < proposals.Count; i++)
                {
                    allProposalsRegions.Add(new ProposalViewModel
                    {
                        Project = proposals[i].Project.Name,
                        WorkTime = proposals[i].WorkTime,
                        Quantity = proposals[i].Quantity,
                        Price = proposals[i].Price,
                        Service = proposals[i].Service.Name,
                        DateFrom = proposals[i].Date,
                        DateTo = proposals[i].Date_to,
                        WorkTimeTo = proposals[i].WorkTimeTo,
                        Id = proposals[i].Id,
                        EditingDate = proposals[i].CreatingDate,
                        Status = proposals[i].Status
                    });
                }
                return allProposalsRegions;
            }
        }

        public List<ProposalViewModel> GetAllProposalsProjects(string email)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                List<ProposalViewModel> allProposalsRegions = new List<ProposalViewModel>();
                var id = ctx.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();

                string id_val = id.Id;

                var project_id = ctx.AspNetUsers.Where(x => x.Id == id_val).FirstOrDefault();

                int? reg_id = project_id.Project_Id;
                var proposals = ctx.Proposals.Where(x => x.Project.City.Region.Id == reg_id).ToList();
                for (int i = 0; i < proposals.Count; i++)
                {
                    allProposalsRegions.Add(new ProposalViewModel
                    {
                        Project = proposals[i].Project.Name,
                        WorkTime = proposals[i].WorkTime,
                        Quantity = proposals[i].Quantity,
                        Price = proposals[i].Price,
                        Service = proposals[i].Service.Name,
                        DateFrom = proposals[i].Date,
                        DateTo = proposals[i].Date_to,
                        WorkTimeTo = proposals[i].WorkTimeTo,
                        Id = proposals[i].Id,
                        EditingDate = proposals[i].CreatingDate,
                        Status = proposals[i].Status
                    });
                }
                return allProposalsRegions;
            }
        }

        public string DeleteProposal(int id)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var prop = ctx.Proposals.Find(id);
                ctx.Proposals.Remove(prop);
                ctx.SaveChanges();
                message = "Status Changed";
                return message;
            }
        }

        public string AddProposal(int project_id, string worktime, string worktimeto, int quantity, int service_id, string date_to, string date_from, string rate, string status)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                decimal rate_new = decimal.Parse(rate);
                DateTime DateTo = DateTime.Parse(date_to);
                DateTime DateFrom = DateTime.Parse(date_from);
                DateTime timefrom = DateTime.Parse(worktime);
                DateTime timeto = DateTime.Parse(worktimeto);
                TimeSpan difference = timeto - timefrom;
                decimal diffInHours = (decimal)difference.TotalHours;
                Proposal prop = new Proposal()
                {
                    Project_id = project_id,
                    WorkTime = worktime,
                    Quantity = quantity,
                    Service_Id = service_id,
                    Date = DateFrom,
                    Date_to = DateTo,
                    Price = diffInHours * quantity * rate_new,
                    WorkTimeTo = worktimeto,
                    CreatingDate = DateTime.Now,
                    Status = status

                };
                ctx.Proposals.Add(prop);
                ctx.SaveChanges();
                message = "Saved with range of dates";
                return message;
            }
        }
    }
}

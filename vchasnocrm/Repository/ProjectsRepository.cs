﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vchasnocrm.Models;
using vchasnocrm.ViewModels;

namespace vchasnocrm.Repository
{
    public class ProjectsRepository
    {
        public List<ProjectViewModel> GetAllProjects()
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                List<ProjectViewModel> allProjects = new List<ProjectViewModel>();
                var projects = ctx.Projects.ToList();
                for (int i = 0; i < projects.Count; i++)
                {
                    allProjects.Add(new ProjectViewModel
                    {
                        Name = projects[i].Name,
                        CityName = projects[i].City.Name,
                        Id = projects[i].Id
                    });
                }
                return allProjects;
            }
        }


        public string UpdateProject(int id, string name)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var prj = ctx.Projects.Find(id);
                prj.Name = name;
                ctx.SaveChanges();
                message = "Updated";
                return message;
            }
        }


        public string AddProject(string name, int city_id)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                Project prj = new Project()
                {
                    Name = name,
                    City_Id = city_id
                };
                ctx.Projects.Add(prj);
                ctx.SaveChanges();
                message = "Updated";
                return message;
            }
        }

        public string DeleteProject(int id)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var prj = ctx.Projects.Find(id);
                ctx.Projects.Remove(prj);
                ctx.SaveChanges();
                message = "Updated";
                return message;
            }
        }
    }
}
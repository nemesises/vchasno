﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vchasnocrm.Models;
using vchasnocrm.ViewModels;

namespace vchasnocrm.Repository
{
    public class AreaRepository
    {
        public List<AreaViewModel> GetAllAreas()
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                List<AreaViewModel> allAreas = new List<AreaViewModel>();
                var areas = ctx.Areas.ToList();
                for (int i = 0; i < areas.Count; i++)
                {
                    allAreas.Add(new AreaViewModel
                    {
                        Name = areas[i].Name,
                        CountryName = areas[i].Country.Name,
                        Id = areas[i].Id
                    });
                }
                return allAreas;
            }
        }
        public string AddArea(string name, int country_id)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
               Area area = new Area()
                {
                    Name = name,
                    Country_id = country_id
                };
                ctx.Areas.Add(area);
                ctx.SaveChanges();
                message = "Added";
                return message;
            }
        }
    }
}
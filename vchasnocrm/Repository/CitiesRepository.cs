﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vchasnocrm.Models;
using vchasnocrm.ViewModels;

namespace vchasnocrm.Repository
{
    public class CitiesRepository
    {
        public List<CityViewModel> GetAllCities()
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                List<CityViewModel> allCities = new List<CityViewModel>();
                var cities = ctx.Cities.ToList();
                for (int i = 0; i < cities.Count; i++)
                {
                    allCities.Add(new CityViewModel
                    {
                        Name = cities[i].Name,
                        RegionName = cities[i].Region.Name,
                        Id = cities[i].Id
                    });
                }
                return allCities;
            }
        }

        public string UpdateCity(int id, string name)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var city = ctx.Cities.Find(id);
                city.Name = name;
                ctx.SaveChanges();
                message = "Updated";
                return message;
            }
        }


        public string AddCity(string name, int region_id)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                City city = new City()
                {
                    Name = name,
                    Region_Id = region_id
                };
                ctx.Cities.Add(city);
                ctx.SaveChanges();
                message = "Added";
                return message;
            }
        }

        public string DeleteCity(int id)
        {
            using (var ctx = new vchasnopersonalEntities())
            {
                string message;
                var city = ctx.Cities.Find(id);
                ctx.Cities.Remove(city);
                ctx.SaveChanges();
                message = "Deleted";
                return message;
            }
        }
    }
}
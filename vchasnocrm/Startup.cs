﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(vchasnocrm.Startup))]
namespace vchasnocrm
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

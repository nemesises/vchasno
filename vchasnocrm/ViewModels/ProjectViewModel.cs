﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vchasnocrm.ViewModels
{
    public class ProjectViewModel
    {
        public string Name { get; set; }
        public string CityName { get; set; }
        public int Id { get; set; }
    }
}
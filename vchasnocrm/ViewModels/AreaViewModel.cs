﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vchasnocrm.ViewModels
{
    public class AreaViewModel
    {
        public string Name { get; set; }
        public string CountryName { get; set; }
        public int Id { get; set; }
    }
}
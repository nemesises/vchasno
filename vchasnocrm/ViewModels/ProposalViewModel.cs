﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vchasnocrm.ViewModels
{
    public class ProposalViewModel
    {
        public string Project {get;set;}
        public string WorkTime {get;set;}
        public int? Quantity {get;set;}
        public decimal? Price {get;set;}
        public string Service {get;set;}
        public DateTime? DateFrom {get;set;}
        public DateTime? DateTo {get;set;}
        public string WorkTimeTo {get;set;}
        public int Id {get;set;}
        public DateTime? EditingDate {get;set;}
        public string Status {get;set;}
        public DateTime? DisablingDate { get; set; }
        public int Fine { get; set; }
        public string City { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vchasnocrm.ViewModels
{
    public class CityViewModel
    {
        public string Name { get; set; }
       
        public string RegionName { get; set; }

        public int Id { get; set; }
    }
}
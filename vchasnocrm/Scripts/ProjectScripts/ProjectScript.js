﻿$(document).ready(function () {
    $("#projectCreate").click(function () {
        AddProject();
    });
    $("#showProjects").click(function () {
        GetProject();
    });
    $('#projects').on('click', 'button.btn', function () {
        var proposalurl = '/projects/deleteproject';
        var id = $(this).closest('tr').find('.projectId').text();

        model = {
            id: parseInt(id)

        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Alert');
                    location.reload();
                }
            });
    });
    $('#projects').on('click', 'button.btn-warning', function () {
        var id = $(this).closest('tr').find('.cityId').text();
     

    });

});


function UpdateProject(idvalue){
    var updateurl = '/projects/updateproject';
    model = {
        id:idvalue,
        name:name
    },
    $.ajax({
        url: updateurl,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(model),
        type: 'POST',
        dataType: 'json',
        processData: false,
        success: function (data) {
            alert('Updated');
            location.reload();
        }
    });
}



function GetProject() {
    var citiesurl = '/projects/index';
    $.ajax({
        url: citiesurl,
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        processData: false,
        success: function (data) {
            $("#projects").empty();
            var list = data;
            console.dir(list);
            for (var i = 0; i <= list.length - 1; i++) {
                var tableData = '<tr>' + '<td>' +
                    (i + 1) +
                    '</td>' +
                    '<td class="projectId" style="display:none">' +
                    list[i].Id +
                    '</td>' +
                    '<td> ' +
                    list[i].Name +
                    '</td>' +
                    '<td > ' +
                    list[i].CityName +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-warning" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Редактировать' + '</button>' +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-delete" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Удалить' + '</button>' +
                    '</td>' +


                     '</tr>';
                $('#projects').append(tableData);
            }
        }
    })
}

function AddProject() {
    var proposalurl = '/projects/addproject';
    var projectName = $('#ProjectName').val();
    var cityId = $('#cityId').val();
    model = {
        name: projectName,
        city_id: parseInt(cityId)
   
    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });

}
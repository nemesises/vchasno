﻿
$(document).ready(function () {
    $("#cityCreate").click(function () {
        AddCity();
    });
    $("#showCities").click(function () {
        GetCity();
    });
    $('#cities').on('click', 'button.btn', function () {
        let id = $(this).closest('tr').find('.cityId').text();
        let proposalurl = '/cities/deletecity';

        model = {
            id: parseInt(id)

        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Deleted');
                    location.reload();
                }
            });
    });

    $('#cities').on('click', 'button.btn-warning', function () {
        let id = $(this).closest('tr').find('.cityId').text();
        alert(id);

    });
});


function UpdateCity(idvalue){
    let updateurl = '/cities/updatecity';
    model = {
        id:idvalue,
        name:name
    },
    $.ajax({
        url: updateurl,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(model),
        type: 'POST',
        dataType: 'json',
        processData: false,
        success: function (data) {
            alert('Updated');
            location.reload();
        }
    });
}

function GetCity() {
    let citiesurl = '/cities/index';
    $.ajax({
        url: citiesurl,
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        processData: false,
        success: function (data) {
            $("#cities").empty();
            var list = data;
            for (var i = 0; i <= list.length - 1; i++) {
                var tableData = '<tr>' + '<td>' +
                    (i + 1) +
                    '</td>' +
                    '<td class="cityId" style="display:none">' +
                    list[i].Id +
                    '</td>' +
                    '<td > ' +
                    list[i].Name +
                    '</td>' +
                    '<td > ' +
                    list[i].RegionName +
                    '</td>' +
                        '<td> ' +
                    '<button type="button" class="btn btn-warning" id="delete"  data-city-id="' + list[i].Id +'" >' + 'Редактировать' + '</button>' +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-delete" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Удалить' + '</button>' +
                    '</td>' +
                    '</tr>';
                $('#cities').append(tableData);
            }
        }
    })
}

function AddCity() {
    let proposalurl = '/cities/addcity';
    let cityName = $('#NameCity').val();
    let regionId = $('#regionId').val();
    model = {
        name: cityName,
        region_id: parseInt(regionId)


    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });

}


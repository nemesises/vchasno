﻿
$(document).ready(function () {
    $("#showPeople").click(function () {
        AllPeople();
    });
    $("#peopleCreate").click(function () {
        AddPeople();
    });
    $("#showPeopleProposals").click(function () {
        PeopleAndProposals();
    });
    $("#peopleToProposalCreate").click(function () {
        PeopleToProposal();
    });
    $('#people').on('click', 'button.btn', function () {
        let proposalurl = '/peopleforworks/deletepeople';
        let id = $(this).closest('tr').find('.peopleId').text();
        model = {
            id: parseInt(id)

        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Deleted');
                    location.reload();
                }
            });
    });
    $('#people').on('click', 'button.btn-warning', function () {
        let id = $(this).closest('tr').find('.cityId').text();
        alert(id);

    });
});

function PeopleAndProposals() {
    let getPeopleUrl = '/peopleforworks/peopleinproposals';
    $.ajax({
        url: getPeopleUrl,
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        processData: false,
        success: function (data) {
            $("#peopleProposals").empty();
            var list = data;
            for (var i = 0; i <= list.length - 1; i++) {
                var tableData = '<tr>' + '<td>' +
                    (i + 1) +
                    '</td>' +
                    '<td>' +
                    list[i].FIO +
                    '</td>' +
                    '<td > ' +
                    list[i].Proposal +
                    '</td>' +
                   
                    '</tr>';
                $('#peopleProposals').append(tableData);
            }
        }
    })
}

function PeopleToProposal() {
    let addingurl = '/peopleforworks/addpeopletoproposal';
    let peopleId = $('#peopleIdAdd').val();
    let proposalId = $('#proposalIdAdd').val();
    model = {
        peopleId: parseInt(peopleId),
        proposalId: parseInt(proposalId)
   
    },
        $.ajax({
            url: addingurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });
}

function DeletePeople() {
    let proposalurl = '/peoplefoeworks/deletepeople';
    let id = $(this).closest('td').prev('.peopleId').text();

    model = {
        id: parseInt(id)

    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });

}


function AllPeople() {
    let getPeopleUrl = '/peopleforworks/index';
    $.ajax({
            url: getPeopleUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'GET',
            dataType: 'json',
            processData: false,
        success: function (data) {
            $("#people").empty();
                var list = data;
                for (var i = 0; i <= list.length - 1; i++) {
                    var tableData = '<tr>' + '<td>' +
                        (i + 1) +
                        '</td>' +
                        '<td class="peopleId" style="display:none">' +
                        list[i].Id +
                        '</td>'+
                        '<td > ' +
                        list[i].FIO +
                        '</td>' +
                        '<td > ' +
                        list[i].Position +
                        '</td>' +
                        '<td > ' +
                        moment(list[i].Birthday).format('DD/MM/YYYY') +
                        '</td>' +
                        '<td> ' +
                        list[i].TelephoneNumber +
                        '</td>' +
                        '<td > ' +
                        list[i].WorkTime +
                        '</td>' +
                        '<td> ' +
                        list[i].Adress +
                        '</td>' +
                        '<td> ' +
                        list[i].INN +
                        '</td>' +
                        '<td> ' +
                        list[i].PassportData +
                        '</td>' +
                        '<td> ' +
                        list[i].MedicalBook +
                        '</td>'+
                        '<td> ' +
                        list[i].MedicalBook +
                        '</td>'+
                        '<td> ' +
                        list[i].Region +
                        '</td>'
                        + '<td> ' +
                        '<button type="button" class="btn btn-warning" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Редактировать' + '</button>' +
                        '</td>' +
                        '<td> ' +
                        '<button type="button" class="btn btn-delete" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Удалить' + '</button>' +
                        '</td>'+
                        '</tr>';
                    $('#people').append(tableData);
                }
            }
    })
}


function AddPeople() {
    let proposalurl = '/peopleforworks/addpeople';
    let Name = $('#Name').val();
    let birthday = $('#birthday').val();
    let telephone = $('#telephonepeople').val();
    let workTime = $('#workTimeVal').val();
    let adress = $('#adress').val();
    let passportData = $('#passportData').val();
    let medicalCard = $('#medicalCard').val();
    let INN = $('#INN').val();
    let medicalbookdate = $('#bookending').val();
    let position = $('#position').val();
    let region = $('#region').val();
    
    model = {
        fio: Name,
        birthday: birthday,
        telephone: telephone,
        worktime: workTime,
        adress: adress,
        passportdata: passportData,
        medicalbook: medicalCard,
        INN: INN,
        bookending: medicalbookdate ,
        position: position,
        region: region

    },
    $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
    });

}
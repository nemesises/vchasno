﻿
$(document).ready(function () {
    
    AllProposals();
    CityFilter();
    ServicesFilter();
    ProjectsFilter();


    $("#proposalCreate").click(function () {
        AddProposal();
    });
    $("#showProposals").click(function () {
        AllProposals();
    });
    $('#serviceId').change(function () {
        ServiceChange();
    });
    $('#proposals').on('click', 'button#done', function () {
        var id = $(this).closest('tr').find('.proposalId').text();
        var proposalurl = '/proposals/changestatus';
        model = {
            id: parseInt(id),
            status: "Подтверждено менеджером Vchasno"
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Подтверждено');
                    location.reload();
                }
            });
    });
    
    $('#proposals').on('click', 'button#reject', function () {
        var id = $(this).closest('tr').find('.proposalId').text();
        var proposalurl = '/proposals/changestatus';
        model = {
            id: parseInt(id),
            status: "Отменено менеджером Vchasno"
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Отклонено');
                    location.reload();
                }
            });
    });


    $('#proposals').on('click', 'button#workers', function () {
        $("#allPeopleProposals").modal();
        var id = $(this).closest('tr').find('.proposalId').text();
        
     
       
        var proposalurl = '/proposalstopeoples/peopleinproposals';
        model = {
            proposalId: parseInt(id),
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
            success: function (data) {
                $('#peopleProposals').empty();
                    var list = data;
                    for (var i = 0; i <= list.length - 1; i++) {
                        var tableData = '<tr>' + '<td>' +
                            (i + 1) +
                            '</td>' +
                            '<td class="peoplePropId" style="display:none"> ' +
                            list[i].Id+
                        '</td>' +
                            '<td> ' +
                          list[i].Name +
                        '</td>' +
                            '<td> ' +
                            list[i].WorkFrom+
                        '</td>' +
                            '<td> ' +
                            list[i].WorkTo+
                        '</td>' +
                            '<td> ' +
                            list[i].BreakFrom +
                        '</td>' +
                            '<td> ' +
                            list[i].BreakTo+
                        '</td>' +
                            '<td> ' +
                            list[i].Comment+
                        '</td>' +
                            '<td> ' +
                            list[i].Status+
                            '</td>' +
                            '<td> ' +
                            '<button type="button" id="addTime" class="btn btn-info">' + 'Добавить время' + '</button>' +
                            '</td>' +
                            '<td> ' +
                            '<button type="button" id="acceptTime" class="btn btn-info">' + 'Подтвердить время' + '</button>' +
                            '</td>' +
                            '</tr>';
                        $('#peopleProposals').append(tableData);
                    }
                }
            });
        
    });

    $('#peopleProposals').on('click', 'button#acceptTime', function () {
        var id = $(this).closest('tr').find('.peoplePropId').text();

        var proposalurl = '/proposalstopeoples/confirmtime';
        model = {
            id: parseInt(id)
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
            success: function (data) {
                alert("Подтверждено");
                location.reload();
            }
            });
    });

    $('#peopleProposals').on('click', 'button#addTime', function () {
        $("#allPeopleProposalsTime").modal();
        var id = $(this).closest('tr').find('.peoplePropId').text();

        $("#timeCreate").click(function () {
            var proposalurl = '/proposalstopeoples/AddInfo';
            model = {
                id: parseInt(id),
                workFrom: $('#workTimeFromProp').val(),
                workTo: $('#workTimeToProp').val(),
                breakFrom: $('#breakTimeFrom').val(),
                breakTo: $('#breakTimeTo').val(),
                Comment: $('#commentProp').val()
            },
                $.ajax({
                    url: proposalurl,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(model),
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    success: function (data) {
                        alert("Добавлено");
                        location.reload();
                    }
                });
        });
    });

    $('#proposals').on('click', 'button#fine', function () {
        $("#allFinesProposals").modal();
        var id = $(this).closest('tr').find('.proposalId').text();
        $('#fineProposals').empty();
        var proposalurl = '/proposals/fineinproposal';
        model = {
            proposalId: parseInt(id),
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
            success: function (data) {

                    var list = data;
                    for (var i = 0; i <= list.length - 1; i++) {
                      var tableData = '<tr>' + '<td>' +
                            (i + 1) +
                            '</td>' +
                            '<td> ' +
                            list[i].Fine +
                            '</td>' +
                            '<td>' +
                            list[i].Price +
                          '</td>' +
                          '<td>' +
                          list[i].Comment +
                          '</td>' +
                            '</tr>';
                        $('#fineProposals').append(tableData);
                    }
                }
            });

    });

 
    $('#statusfilter').change(function () {
        var contentToColor = {
            "Заявка отменена": "#9900ff",
            "Подтверждено менеджером Vchasno": "green",
            "Отменено менеджером Vchasno": "#9900ff",
            "Отклонено региональным менеджером": "#9900ff",
            "Подтверждено региональным менеджером": "red"
        };
        var status = this.value;
        var filter, table, tr, td, i;
        filter = status.toUpperCase();
        table = document.getElementById("proposals");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[7];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
          
        }
    });
    $('#projectfilter').change(function () {
        var contentToColor = {
            "Заявка отменена": "#9900ff",
            "Подтверждено менеджером Vchasno": "green",
            "Отменено менеджером Vchasno": "#9900ff",
            "Отклонено региональным менеджером": "#9900ff",
            "Подтверждено региональным менеджером": "red"
        };
        var project = this.value;
        var filter, table, tr, td, i;
        filter = project.toUpperCase();
        table = document.getElementById("proposals");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }

        }
       
    });
    $('#servicefilter').change(function () {
        var contentToColor = {
            "Заявка отменена": "#9900ff",
            "Подтверждено менеджером Vchasno": "green",
            "Отменено менеджером Vchasno": "#9900ff",
            "Отклонено региональным менеджером": "#9900ff",
            "Подтверждено региональным менеджером": "red"
        };
        var service = this.value;
      
        filter = service.toUpperCase();
        table = document.getElementById("proposals");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[5];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }

        }
    });
    $('#datefilter').change(function () {
        var contentToColor = {
            "Заявка отменена": "#9900ff",
            "Подтверждено менеджером Vchasno": "green",
            "Отменено менеджером Vchasno": "#9900ff",
            "Отклонено региональным менеджером": "#9900ff",
            "Подтверждено региональным менеджером": "red"
        };
        var date = moment(this.value).format('DD/MM/YYYY');
        var filter, table, tr, td, i;
        filter = date.toUpperCase();
        table = document.getElementById("proposals");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }

        }

    });
    $('#cityfilter').change(function () {
        var contentToColor = {
            "Заявка отменена": "#9900ff",
            "Подтверждено менеджером Vchasno": "green",
            "Отменено менеджером Vchasno": "#9900ff",
            "Отклонено региональным менеджером": "#9900ff",
            "Подтверждено региональным менеджером": "red"
        };
        var city = this.value;
        var filter, table, tr, td, i;
        filter = city.toUpperCase();
        table = document.getElementById("proposals");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[10];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }

        }
    });
});


function DeleteProposal() {
    var proposalurl = '/proposals/deleteproposal';
    var id = $(this).closest('td').prev('.proposalId').text();

    model = {
        id: parseInt(id)

    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Alert');
                location.reload();
            }
        });

}


function ServiceChange() {
    var changeUrl = '/proposals/changer';
    var serviceId = $('#serviceId').val();
    model = {
        serviceId:parseInt(serviceId)
    },
        $.ajax({
            url: changeUrl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                $('#rate').text(Math.ceil(parseFloat(data[0].rate)*100)/100);
            }
        });
}

function AddProposal() {
    $(function () {
        $('[type="date"].form-control').prop('min', function () {
            return new Date().toJSON().split('T')[0];
        });
    });
    var proposalurl = '/proposals/addproposal';
    var projectId = $('#projectId').val();
    var dateFrom = $('#datefrom').val();
    var dateTo = $('#dateto').val();
    var serviceId = $('#serviceId').val();
    var workingTime = $('#workTime').val();
    var workingTimeTo = $('#workTimeTo').val();
    var quantity = $('#quantity').val();
    var rate = $('#rate').val();
    var status = "Заявка создана (Vchasno менеджер)"
    model = {
        project_id: parseInt(projectId),
        date_from: dateFrom,
        date_to: dateTo,
        worktime: workingTime,
        worktimeto: workingTimeTo,
        quantity: parseInt(quantity),
        service_id: parseInt(serviceId),
        rate: rate,
        status: status
    },
    $.ajax({
        url: proposalurl,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(model),
        type: 'POST',
        dataType: 'json',
        processData: false,
        success: function (data) {
            alert('Saved');
            location.reload();
        }
    });
   
}


function AllProposals() {
    var contentToColor = {
        "Заявка отменена": "#9900ff",
        "Подтверждено менеджером Vchasno": "green",
        "Отменено менеджером Vchasno": "#9900ff",
        "Отклонено региональным менеджером": "#9900ff",
        "Подтверждено региональным менеджером": "red"
    };
    var getProposalsUrl = '/proposals/index';
    $.ajax({
        url: getProposalsUrl,
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        processData: false,
        success: function (data) {
            $("#proposals").empty();
            var list = data;
            for (var i = 0; i <= list.length - 1; i++) {

               
                var tableData = '<tr>' +
                    '<td class="proposalId">' +
                    list[i].Id +
                    '</td>' +
                    '<td > ' +
                    list[i].Project +
                    '</td>' +
                    '<td > ' +
                    moment(list[i].DateFrom).format('DD/MM/YYYY') + "--" + moment(list[i].DateTo).format('DD/MM/YYYY') +
                    '</td>' +
                    '<td> ' +
                    list[i].WorkTime + "--" +list[i].WorkTimeTo +
                    '</td>' +
                    '<td > ' +
                    list[i].Quantity+
                    '</td>' +
                    '<td> ' +
                    list[i].Service +
                    '</td>' +
                    '<td> ' +
                    list[i].Price +
                    '<td' + (contentToColor[list[i].Status] !== 'undefined' ? ' style="color: ' + contentToColor[list[i].Status] + ';"' : '') + '> ' +
                    list[i].Status +
                    '</td>' +
                    '<td> ' +
                    moment(list[i].EditingDate).format('DD/MM/YYYY HH:mm') +
                    '</td>' +
                    '<td> ' +
                    moment(list[i].DisablingDate).format('DD/MM/YYYY HH:mm') +
                    '</td>' +
                    '<td> ' +
                    list[i].City +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" id="done" class="btn btn-info">' + 'Подтвердить' + '</button>' +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" id="reject" class="btn btn-delete">' + 'Отклонить' + '</button>' +
                    '</td>' +
                    '<td> ' + '<button data-target="#allPeopleProposals"  id="workers">' +
                    '<i  class="fas fa-people-carry  fa-2x"></i>' + '</button>' + '</td>' +
                    '<td> ' + '<button data-target="#finesProposal"  id="fine">' +
                    '<i class="fas fa-exclamation-triangle fa-2x"></i>' + '</button>' + '</td>' +
                    '</tr>';
                $('#proposals').append(tableData);
            }

        }
    })
}

function SendEmail() {
    var sendurl = '/mail/sendemail';

}



function ServicesFilter() {
    var getProposalsUrl = '/services/servicesfilterresult';
    $.ajax({
        url: getProposalsUrl,

        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        dataType: 'json',
        processData: false,
        success: function (data) {
            var list = data;

            $.each(list, function (i, d) {
                $('#servicefilter').append('<option value="' + d.Name + '">' + d.Name + '</option>');
            });
        }
    });
};


function ProjectsFilter() {
    var getProposalsUrl = '/projects/projectsfilterresult';
    $.ajax({
        url: getProposalsUrl,

        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        dataType: 'json',
        processData: false,
        success: function (data) {
            var list = data;

            $.each(list, function (i, d) {
                $('#projectfilter').append('<option value="' + d.Name + '">' + d.Name + '</option>');
            });
        }
    });
};

function CityFilter() {
    var getProposalsUrl = '/cities/citiesfilterresult';
    $.ajax({
        url: getProposalsUrl,

        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        dataType: 'json',
        processData: false,
        success: function (data) {
            var list = data;

            $.each(list, function (i, d) {
                $('#cityfilter').append('<option value="' + d.Name + '">' + d.Name + '</option>');
            });
        }
    });
};



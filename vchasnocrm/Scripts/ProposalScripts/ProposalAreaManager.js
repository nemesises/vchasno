﻿
$(document).ready(function () {

    
    AllProposals();
    $("#proposalCreate").click(function () {
        AddProposal();
    });
    $("#showProposals").click(function () {
        AllProposals();
    });
    $('#serviceId').change(function () {
        ServiceChange();
    });
    $('.btn_hide').click(function () {
        DeleteProposal();
    });

    $('#proposals').on('click', 'button#acceptManager', function () {
        var id = $(this).closest('tr').find('.proposalId').text();
        var proposalurl = '/proposals/changestatus';
        model = {
            id: parseInt(id),
            status: "Подтверждено региональным менеджером"
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Подтверждено');
                    location.reload();
                }
            });
    });


    $('#proposals').on('click', 'button#rejectManager', function () {
        var id = $(this).closest('tr').find('.proposalId').text();
        var proposalurl = '/proposals/changestatus';
        model = {
            id: parseInt(id),
            status: "Отклонено региональным менеджером"
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Отклонено');
                    location.reload();
                }
            });
    });




    $('#proposals').on('click', 'button#workers', function () {
        $("#allPeopleProposals").modal();
        var id = $(this).closest('tr').find('.proposalId').text();



        var proposalurl = '/proposalstopeoples/peopleinproposals';
        model = {
            proposalId: parseInt(id),
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
            success: function (data) {
                $('#peopleProposals').empty();
                    var list = data;
                    for (var i = 0; i <= list.length - 1; i++) {
                        var tableData = '<tr>' + '<td>' +
                            (i + 1) +
                            '</td>' +
                            '<td class="peoplePropId" style="display:none"> ' +
                            list[i].Id +
                            '</td>' +
                            '<td> ' +
                            list[i].Name +
                            '</td>' +
                            '<td> ' +
                            list[i].WorkFrom +
                            '</td>' +
                            '<td> ' +
                            list[i].WorkTo +
                            '</td>' +
                            '<td> ' +
                            list[i].BreakFrom +
                            '</td>' +
                            '<td> ' +
                            list[i].BreakTo +
                            '</td>' +
                            '<td> ' +
                            list[i].Comment +
                            '</td>' +
                            '<td> ' +
                            list[i].Status +
                            '</td>' +
                            '<td> ' +
                            '<button type="button" id="addTime" class="btn btn-info">' + 'Добавить время' + '</button>' +
                            '</td>' +
                            '<td> ' +
                            '<button type="button" id="acceptTime" class="btn btn-info">' + 'Подтвердить время' + '</button>' +
                            '</td>' +
                            '</tr>';
                        $('#peopleProposals').append(tableData);
                    }
                }
            });

    });

    $('#peopleProposals').on('click', 'button#acceptTime', function () {
        var id = $(this).closest('tr').find('.peoplePropId').text();

        var proposalurl = '/proposalstopeoples/confirmtime';
        model = {
            id: parseInt(id)
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert("Подтверждено");
                    location.reload();
                }
            });
    });

    $('#peopleProposals').on('click', 'button#addTime', function () {
        $("#allPeopleProposalsTime").modal();
        var id = $(this).closest('tr').find('.peoplePropId').text();

        $("#timeCreate").click(function () {
            var proposalurl = '/proposalstopeoples/AddInfo';
            model = {
                id: parseInt(id),
                workFrom: $('#workTimeFromProp').val(),
                workTo: $('#workTimeToProp').val(),
                breakFrom: $('#breakTimeFrom').val(),
                breakTo: $('#breakTimeTo').val(),
                Comment: $('#commentProp').val()
            },
                $.ajax({
                    url: proposalurl,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(model),
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    success: function (data) {
                        alert("Добавлено");
                        location.reload();
                    }
                });
        });
    });

    $('#proposals').on('click', 'button#fine', function () {
        $("#allFinesProposals").modal();
        var id = $(this).closest('tr').find('.proposalId').text();
        $('#fineProposals').empty();
        var proposalurl = '/proposals/fineinproposal';
        model = {
            proposalId: parseInt(id),
        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    var list = data;
                    for (var i = 0; i <= list.length - 1; i++) {
                        var tableData = '<tr>' + '<td>' +
                            (i + 1) +
                            '</td>' +
                            '<td> ' +
                            list[i].Fine
                        '</td>' +
                            '<td>' +
                            list[i].Fine +
                            '</td>' +
                            '</tr>';
                        $('#fineProposals').append(tableData);
                    }
                }
            });

    });
});

$(document).on('click', '#addShowArea', function () {
    ShowAndPopulateArea();
    ShowAndPopulateService();
});
function DeleteProposal() {
    var proposalurl = '/proposals/deleteproposal';
    var id = $(this).closest('td').prev('.proposalId').text();

    model = {
        id: parseInt(id)

    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });

}

function ServiceChange() {
    var changeUrl = '/proposals/changer';
    var serviceId = $('#serviceId').val();
    model = {
        serviceId: parseInt(serviceId)
    },
        $.ajax({
            url: changeUrl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                $('#rate').text(Math.ceil(parseFloat(data[0].rate) * 100) / 100);
            }
        });
}

function AddProposal() {
    $(function () {
        $('[type="date"].form-control').prop('min', function () {
            return new Date().toJSON().split('T')[0];
        });
    });
    var proposalurl = '/proposals/addproposal';
    var projectId = $('#projectId').val();
    var dateFrom = $('#datefrom').val();
    var dateTo = $('#dateto').val();
    var serviceId = $('#serviceId').val();
    var workingTime = $('#workTime').val();
    var workingTimeTo = $('#workTimeTo').val();
    var quantity = $('#quantity').val();
    var rate = $('#rate').val();
    var status = "Заявка создана (Территориальный менеджер)"
    model = {
        project_id: parseInt(projectId),
        date_from: dateFrom,
        date_to: dateTo,
        worktime: workingTime,
        worktimeto: workingTimeTo,
        quantity: parseInt(quantity),
        service_id: parseInt(serviceId),
        rate: rate,
        status:status
    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });

}

//TODO End this request to get all proposals
function AllProposals() {
    var email = $('#userId').text();

    var getProposalsUrl = '/proposals/proposalarea';
    model = {
        email: email
    },
        $.ajax({
            url: getProposalsUrl,
            data: JSON.stringify(model),
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                $("#proposals").empty();
                var list = data;
                for (var i = 0; i <= list.length - 1; i++) {
                    var tableData = '<tr>' +
                        '<td class="proposalId">' +
                        list[i].Id +
                        '</td>' +
                        '<td > ' +
                        list[i].Project +
                        '</td>' +
                        '<td > ' +
                        moment(list[i].DateFrom).format('DD/MM/YYYY') + "--" + moment(list[i].DateTo).format('DD/MM/YYYY') +
                        '</td>' +
                        '<td> ' +
                        list[i].WorkTime + "--" + list[i].WorkTimeTo +
                        '</td>' +
                        '<td > ' +
                        list[i].Quantity +
                        '</td>' +
                        '<td> ' +
                        list[i].Service +
                        '</td>' +
                        '<td> ' +
                        list[i].Price +
                        '</td>' +
                        '<td> ' +
                        moment(list[i].EditingDate).format('DD/MM/YYYY') +
                        '</td>' +
                        '<td> ' +
                        list[i].Status +
                        '</td>' +
                        '<td> ' +
                        '<button type="button" id="acceptManager" class="btn btn-info">' + 'Подтвердить' + '</button>' +
                        '</td>' +
                        '<td> ' +
                        '<button type="button" id="rejectManager" class="btn btn-delete">' + 'Отклонить' + '</button>' +
                        '</td>' +
                        '<td> ' + '<button data-target="#allPeopleProposals"  id="workers">' +
                        '<i  class="fas fa-people-carry  fa-2x"></i>' + '</button>' + '</td>' +
                        '<td> ' + '<button data-target="#finesProposal"  id="fine">' +
                        '<i class="fas fa-exclamation-triangle fa-2x"></i>' + '</button>' + '</td>' +
                        '</tr>';
                    $('#proposals').append(tableData);
                }

            }
        })
}

function SendEmail() {
    var sendurl = '/mail/sendemail';

}

function ShowAndPopulateArea() {

    $('#addProposalManagerArea').load('/Manage/AddProposalManagerArea', function () {

        var email = $('#userId').text();
        var getProposalsUrl = '/manage/populateprojectsarea';
        model = {
            email: email
        },
            $.ajax({
                url: getProposalsUrl,
                data: JSON.stringify(model),
                contentType: 'application/json; charset=utf-8',
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    var list = data;

                    $.each(list, function (i, d) {
                        $('#projectIdArea').append('<option value="' + d.Id + '">' + d.Name + '</option>');
                    });
                }
            })
    });

}
function ShowAndPopulateService() {
    $('#addProposalManagerArea').load('/Manage/AddProposalManagerArea', function () {

        var email = $('#userId').text();
        var getProposalsUrl = '/manage/populateservicearea';
        model = {
            email: email
        },
            $.ajax({
                url: getProposalsUrl,
                data: JSON.stringify(model),
                contentType: 'application/json; charset=utf-8',
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    var list = data;

                    $.each(list, function (i, d) {
                        $('#serviceId').append('<option value="' + d.Id + '">' + d.Name + '</option>');
                    });
                }
            })
    });
}
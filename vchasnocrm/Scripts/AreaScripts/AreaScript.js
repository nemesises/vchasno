﻿$(document).ready(function () {
 $("#areaCreate").click(function () {
        AddArea();

    });
});


function AddArea() {
    let proposalurl = '/areas/addarea';
    let areaName = $('#NameArea').val();
    let countryId = $('#countryId').val();
    model = {
        name: areaName,
        country_id: parseInt(countryId)
    },
    $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
    });

}

﻿$(document).ready(function () {
 $("#countryCreate").click(function () {
        AddCountry();
    });
});


function AddCountry() {
    let proposalurl = '/countries/addcountry';
    let countryName = $('#NameCountry').val();
    model = {
        name: countryName,
    },
    $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
    });

}
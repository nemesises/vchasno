﻿$(document).ready(function () {
    $("#serviceCreate").click(function () {
        AddService();
    });
    $("#showServices").click(function () {
        GetServices();
    });
    $('#services').on('click', 'button.btn', function () {
        var id = $(this).closest('tr').find('.serviceId').text();
        var serviceurl = '/services/deleteservice';

        model = {
            id: parseInt(id)

        },
            $.ajax({
                url: serviceurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Alert');
                    location.reload();
                }
            });
    });
    $('#services').on('click', 'button.btn-warning', function () {
        var id = $(this).closest('tr').find('.cityId').text();
        

    });
});

function GetServices() {
    var servicesurl = '/services/index';
    $.ajax({
        url: servicesurl,
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        processData: false,
        success: function (data) {
            $("#services").empty();
            var list = data;
            for (var i = 0; i <= list.length - 1; i++) {
                var tableData = '<tr>' + '<td>' +
                    (i + 1) +
                    '</td>' +
                    '<td class="serviceId" style="display:none">' +
                    list[i].Id +
                    '</td>' +
                    '<td > ' +
                    list[i].Name +
                    '</td>' +
                    '<td > ' +
                    list[i].Price +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-warning" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Редактировать' + '</button>' +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-delete" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Удалить' + '</button>' +
                    '</td>' +
                    
                    '</tr>';
                $('#services').append(tableData);
            }
        }
    })
}


function AddService() {
    var proposalurl = '/services/addservice';
    var serviceName = $('#NameService').val();
    var fee = $('#Fee').val().replace(",", ".");
    var country_id = $('#countryIdService').val();
    model = {
        name: serviceName,
        fee: fee,
        country_id:parseInt(country_id)
    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });

}
﻿
$(document).ready(function () {
    $("#regionCreate").click(function () {
        AddRegion();
    });
    $("#showRegions").click(function () {
        GetRegions();
    });

    $('#regions').on('click', 'button.btn', function () {
        let proposalurl = '/regions/deleteregion';
        let id = $(this).closest('tr').find('.regionId').text();
        model = {
            id: parseInt(id)

        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Alert');
                    location.reload();
                }
            });

    });
    $('#regions').on('click', 'button.btn-warning', function () {
        let id = $(this).closest('tr').find('.cityId').text();
        alert(id);

    });
});

function UpdateRegion(idvalue){
    let updateurl = '/regions/updateregion';
    model = {
        id:idvalue,
        name:name
    },
    $.ajax({
        url: updateurl,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(model),
        type: 'POST',
        dataType: 'json',
        processData: false,
        success: function (data) {
            alert('Updated');
            location.reload();
        }
    });
}



function GetRegions() {
    let regionsurl = '/regions/index';
    $.ajax({
        url: regionsurl,
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        processData: false,
        success: function (data) {
            $("#regions").empty();
            var list = data;
            for (var i = 0; i <= list.length - 1; i++) {
                var tableData = '<tr>' + '<td>' +
                    (i + 1) +
                    '</td>' +
                    '<td class="regionId" style="display:none">' +
                    list[i].Id +
                    '</td>' +
                    '<td > ' +
                    list[i].Name +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-warning" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Редактировать' + '</button>' +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-delete" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Удалить' + '</button>' +
                    '</td>' +
                     '</tr>';
                $('#regions').append(tableData);
            }
        }
    })
}

function AddRegion() {
    let proposalurl = '/regions/addregion';
    let regionName = $('#NameRegion').val();
    let areaId = $('#areaId').val();
    model = {
       name: regionName,
       areaId: parseInt(areaId)
    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });

}

﻿$(document).ready(function () {

    $("#fineCreate").click(function () {
        AddFine();
    });
    $("#showFines").click(function () {
        GetFines();
    });
    $('#fines').on('click', 'button.btn', function () {
        let id = $(this).closest('tr').find('.fineId').text();
        let proposalurl = '/fines/deletefine';

        model = {
            id: parseInt(id)

        },
            $.ajax({
                url: proposalurl,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(model),
                type: 'POST',
                dataType: 'json',
                processData: false,
                success: function (data) {
                    alert('Deleted');
                    location.reload();
                }
            });
    });
    $('#fines').on('click', 'button.btn-warning', function () {
        let id = $(this).closest('tr').find('.cityId').text();
        alert(id);

    });
});

function AddFine() {
    let proposalurl = '/fines/addfine';
    let fineName = $('#NameFine').val();
    let FinePrice = $('#PriceFine').val();
    let country_Id = $('#countryIdFine').val();
   
    model = {
        name: fineName,
        price: parseFloat(FinePrice),
        country_Id : parseInt(country_Id)

    },
        $.ajax({
            url: proposalurl,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(model),
            type: 'POST',
            dataType: 'json',
            processData: false,
            success: function (data) {
                alert('Saved');
                location.reload();
            }
        });

}


function GetFines() {
    let citiesurl = '/fines/index';
    $.ajax({
        url: citiesurl,
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        processData: false,
        success: function (data) {
            $("#fines").empty();
            var list = data;
            for (var i = 0; i <= list.length - 1; i++) {
                var tableData = '<tr>' + '<td>' +
                    (i + 1) +
                    '</td>' +
                    '<td class="fineId" style="display:none">' +
                    list[i].Id +
                    '</td>' +
                    '<td > ' +
                    list[i].Name + 
                    '</td>' +
                    '<td > ' +
                    list[i].Price +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-warning" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Редактировать' + '</button>' +
                    '</td>' +
                    '<td> ' +
                    '<button type="button" class="btn btn-delete" id="delete"  data-city-id="' + list[i].Id + '" >' + 'Удалить' + '</button>' +
                    '</td>' +
                    '</tr>';
                $('#fines').append(tableData);
            }
        }
    })
}